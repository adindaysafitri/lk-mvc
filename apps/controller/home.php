<?php

class home extends Controller {

    private $dt;
    private $df;

    public function __construct() {
        $this->dt = $this->loadModel('mBarang'); //$dt akan berisi object dari model/barang.php
        $this->df = $this->loadModel('mDaftarBarang');
    }

    public function index() {
        echo "Anda memanggil action index \n";
    }

    public function home($data1, $data2) {
        echo "Anda memanggil action home dengan data 1 = $data1 dan data 2 = $data2 \n";
    }

    public function lihatData($id){
        $data = $this->df->getDataById($id);

        $this->loadView('template/header', ['title'=>'Detail Barang']);
        $this->loadView('/home/vDetailBarang', $data);
        $this->loadView('template/footer', $data);
    }

    public function listBarang(){
        $data = $this->df->getDataAll();

        $this->loadView('template/header', ['title'=>'List Barang']);
        $this->loadView('/home/vDaftarBarang', $data);
        $this->loadView('template/footer', $data);
    }  
    
    public function insertBarang(){
        if (!empty($_POST)) {
            if ($this->df->tambahBarang($_POST)){
                header('Location: '.BASE_URL.'index.php?r=home/listBarang');
                exit;
            }
        }

        $this->loadView('template/header', ['title'=>'Insert Barang']);
        $this->loadView('home/vInsert');
        $this->loadView('template/footer');
    }

    public function updateBarang($id){
        $data = $this->df->getDataById($id);

        if (!empty($_POST)) {
            if ($this->df->updateBarang($_POST)){
                header('Location: '.BASE_URL.'index.php?r=home/listBarang');
                exit;
            }
        }

        $this->loadView('template/header', ['title'=>'Insert Barang']);
        $this->loadView('home/vUpdate', $data);
        $this->loadView('template/footer');
    }

    public function deleteBarang($id){
        $data = $this->df->getDataById($id);

        if ($this->df->deleteBarang($_POST)){
                header('Location: '.BASE_URL.'index.php?r=home/listBarang');
                exit;
        }
    }
}