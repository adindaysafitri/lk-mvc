<?php

require "config.php";
require "Controller.php";
require "Database.php";
require "Model.php";
class Boot {

    protected $controller = 'index';
    protected $action = 'index';
    protected $param = [];
    public function __construct(){
        //echo "Booting sukses";

        $url = $_GET['r'];
        $url = $this->parseUrl($url);//manggil parseUrl
        
        if (file_exists('apps/controller/'.$url[0].'.php')) {
            $this->controller = $url[0];
            unset($url[0]);
        }

        require ('apps/controller/'.$this->controller.'.php');
        $this->controller = new $this->controller;

        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->action = $url[1];
                unset($url[1]);
            }
        }
        if(!empty($url)) {
            $this->param = array_values($url);
        }
        call_user_func_array([ $this->controller, $this->action], $this->param);
    }

    //routing aplikasi MVC -> yang di url namanya diganti 
    //localhost/MVC_(CRUD)/index.php?r=controller/param1/param2
    public function parseUrl($url){
        
        if (isset($_GET['r'])){
            //apabila user menginputkan kelebihan tanda '/' otomatis di-trim biar outputnya ga null
            $url = rtrim($_GET['r'], '/');
            //melindungi url dari Sting yang berbahaya
            $url = filter_var($url, FILTER_SANITIZE_URL);
            //memecah String url berdasarkan '/'
            $url = explode('/', $url);
        }
        return $url;
    }
}