<?php

class Controller{
    
    //handle model 
    public function loadModel($model){
        if (file_exists('apps/model/'.$model.'.php')){
            require_once('apps/model/'.$model.'.php');
            $model = new $model();
        }
        return $model;
    }

    public function loadView($view, $data=null){
        if (file_exists('apps/view/'.$view.'.php')){
            require_once('apps/view/'.$view.'.php');
        }
    }
}