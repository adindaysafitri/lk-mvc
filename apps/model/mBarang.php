<?php

class mBarang extends Model {
    private $id;
    private $nama;
    private $qty;

    public function __construct() {
        $this->id = "A01";
        $this->nama = "Beras";
        $this->qty = "100";
    }
    public function getData(){
        return "Data yang diminta dr model barang : $this->id, $this->nama, $this->qty";
    }

    public function getDataOne(){
        $data = [
            'id'=> $this->id,
            'nama'=> $this->nama,
            'qty'=> $this->qty,
        ];
        return $data;
    }

}